package com.example.fastclickergame;

import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button b_start, b_click, b_click1;
    TextView tv_time, tv_clicks;
    int numberOfClicks = 0;
    int secondsLeft = 20;

    CountDownTimer timer = new CountDownTimer(20000, 1000) {
        @Override
        public void onTick(long millisUntilFinished) {
            secondsLeft--;
            tv_time.setText("Seconds Left: "+secondsLeft);

        }

        @Override
        public void onFinish() {
            b_click1.setEnabled(false);
            b_click.setEnabled(false);
            b_start.setEnabled(true);

        }
    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b_start = (Button) findViewById(R.id.b_start);
        b_click = (Button) findViewById(R.id.b_click);
        b_click1 = (Button) findViewById(R.id.b_click1);

        tv_time = (TextView) findViewById(R.id.tv_time);
        tv_clicks = (TextView) findViewById(R.id.tv_clicks);

        b_click.setEnabled(false);
        b_click1.setEnabled(false);

        b_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                numberOfClicks++;
                tv_clicks.setText("Total Clicks: "+numberOfClicks);
                b_click.setEnabled(false);
                b_click1.setEnabled(true);
            }
        });
        b_click1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                numberOfClicks++;
                tv_clicks.setText("Total Clicks: "+numberOfClicks);
                b_click1.setEnabled(false);
                b_click.setEnabled(true);
            }
        });

        b_start.setOnClickListener( new View.OnClickListener(){
            @Override
            public  void onClick(View view){
                b_start.setEnabled(false);
                secondsLeft = 20;
                numberOfClicks = 0;
                b_click1.setEnabled(true);
                timer.start();
            }
        });


    }
}
